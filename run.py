from bs4 import BeautifulSoup
from selenium import webdriver
import time

start_time = time.time()

#browser settings
options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument('--incognito')
options.add_argument('--headless')
driver = webdriver.Chrome(options=options)


#site and more buttons
driver.get("https://www.employment.kg/vacancies")
more_button = driver.find_element_by_class_name("show_more")
while more_button.is_displayed():
    driver.execute_script("arguments[0].click();", more_button)
page_source = driver.page_source


#soup
soup = BeautifulSoup(page_source, 'lxml')
container = soup.find('div', attrs={'class': 'filtered_container'})
page_block = container.find_all('ul', recursive=False)



def open_vac(item):
    link = item.find('a').get('href')
    id_ = link.split('/')[-1] #vac link id
    vac = item.find('div', attrs={'class': 'name'}).find('p').string
    company = item.find('div', attrs={'class': 'name'}).find('span').string
    city = item.find('div', attrs={'class': 'maps'}).find('span').string
    involvement = item.find('div', attrs={'class': 'time'}).find('div').find('span').string
    print('\n\n')
    print(link)
    print(id_)
    print(vac)
    print(company)
    print(city)
    print(involvement)



print('\n\nit took', format(time.time() - start_time, '.2f'), '')
print(len(page_block))


driver.delete_all_cookies()
driver.quit()
